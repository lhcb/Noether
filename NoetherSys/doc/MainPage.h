/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor noetherdoxygenmain
 *
 * This reference manual documents all %LHCb and %Gaudi classes accessible from
 * the Noether applications environment. More information on Noether
 * applications, including a list of available releases, can be found in the
 * <a href=" http://cern.ch/lhcb-release-area/DOC/noether/">Noether project Web pages</a>
 *
 * <hr>
 * \htmlinclude new_release.notes

 *
 */
