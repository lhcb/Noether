/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SRC_FAKEINDEXERDATA_H
#define SRC_FAKEINDEXERDATA_H 1
// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IIncidentSvc.h"

namespace EventIndexerTest {
  /** @class FakeIndexerData FakeIndexerData.h src/FakeIndexerData.h
   *
   * Generate fake data to test the EventIndexer algorithm.
   *
   * @author Marco Clemencic
   * @date 14/03/2013
   */
  class FakeIndexerData : public GaudiAlgorithm {
  public:
    /// Standard constructor
    FakeIndexerData( const std::string& name, ISvcLocator* pSvcLocator );

    virtual StatusCode initialize() override; ///< Algorithm initialization
    virtual StatusCode execute() override;    ///< Algorithm execution
    virtual StatusCode finalize() override;   ///< Algorithm finalization
  protected:
  private:
    SmartIF<IIncidentSvc> m_incSvc;
    long long             m_eventNumber;

    // Name of the fake input files (10 events each).
    std::vector<std::string> m_files;
  };

} // namespace EventIndexerTest

#endif // SRC_FAKEINDEXERDATA_H
