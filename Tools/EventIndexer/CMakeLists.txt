###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package    : Tools/EventIndexer
# Maintainer : Marco Clemencic
################################################################################
gaudi_subdir(EventIndexer v1r3)

gaudi_depends_on_subdirs(GaudiAlg Event/RecEvent Event/HltEvent)

find_package(ROOT COMPONENTS Core Tree)
find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(EventIndexer
                 src/*.cpp
                 INCLUDE_DIRS ROOT
                 LINK_LIBRARIES ROOT GaudiAlgLib RecEvent HltEvent)


gaudi_add_dictionary(EventIndexer
                     src/EventIndexer.h dict/selection.xml
                     LINK_LIBRARIES ROOT GaudiAlgLib RecEvent HltEvent
                     INCLUDE_DIRS ROOT
                     OPTIONS "-Wno-unknown-attributes")

gaudi_add_test(QMTest QMTEST)
