###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###################
# Define InputData
###################

from PRConfig import TestFileDB
TestFileDB.test_file_db["R08S14_smallfiles"].run()

from Configurables import FileMerger
FileMerger().OutputFile = "MergedSmallFiles.dst"
##############################################
#Debug printout, lists all cleaned directories
##############################################

from Configurables import FSRCleaner
FSRCleaner().OutputLevel = 2
#FSRCleaner().Enable=False

##############################
#fill summary every event
##############################

from Configurables import LHCbApp
LHCbApp().XMLSummary = "summary.xml"
from Configurables import XMLSummarySvc
XMLSummarySvc("CounterSummarySvc").UpdateFreq = 1
